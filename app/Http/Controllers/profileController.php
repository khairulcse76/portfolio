<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class profileController extends Controller
{
    public function viewProfile(){

        return view("admin/userProfile");
    }
    public function insertProfile(){

        return view("admin/insertUserProfile");
    }
}
