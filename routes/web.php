<?php


use \App\Post;
use \App\User;

/*
|--------------------------------------------------------------------------
| User Route
|--------------------------------------------------------------------------
*/
Route::get('/user/create', function (){
    $user=new User();
    $user->name='Rejuanul';
    $user->email='Rejuanul@gmail.com';
    $user->password='15486';
    $user->save();
    return 'success';

});
Route::get('/user/{id}/post', function ($id){

    $posts= User::find($id)->Post->body;

    return $posts;
});
Route::get('/post/{id}/user', function ($id){

    $user= Post::find($id)->User->name;

    return $user;
});

Route::get('/post/{id}', function ($id){

    $posts= User::find($id);

    foreach ($posts->Posts as $post){
        echo $post->title." =>".$post->body."<br>";
    }

});
/*
|--------------------------------------------------------------------------
| Udemy Routes
|--------------------------------------------------------------------------
*/

Route::get('/udemy/insert', function () {

    $posts=DB::insert("INSERT INTO `posts` (`id`,`user_id`, `title`, `body`, `created_at`, `updated_at`)
            VALUES ('7','1', 'nothing', 'everything is done fourth time insert by cgit a', '2017-08-02 00:00:00', '2017-08-25 00:00:00')");
return "inserted data";
});
Route::get('/udemy/read', function () {

    $posts=Post::all();
//    return $posts->title." ".$posts->body;
//    foreach($posts as $post){
//        return $post;
//    }
    return $posts;
});

Route::get('/udemy/where', function () {

    $posts=Post::where('id',2)->get();

    return $posts;

});


Route::get('/udemy/update', function () {

    $post=new Post();
//    $post=Post::find(1);

    $post->title='C++';
    $post->user_id='2';
    $post->body='C++ mother language of programming';
    $post->save();
    return 'Data Updated';

});
Route::get( 'udemy/create', function (){
    Post::create(['title'=>'sahid', 'body'=>'shadin akhane ase']);
});



Route::get( 'udemy/delete', function (){
//    $post=post::find(14);
//    $post->delete();

    Post::destroy(11);
});

Route::get( 'udemy/delete', function (){
//    $post=post::find(14);
//    $post->delete();

    Post::destroy(11);
});
Route::get( 'udemy/softdelete', function (){
    post::find(3)->delete();
});

Route::get( 'udemy/readTrashed', function (){
//    $post=Post::withTrashed()->where('is_admin', 0)->get();
    $post=Post::onlyTrashed()->get();

    return $post;
});
Route::get( 'udemy/restore', function (){
    $post=Post::withTrashed()->where('is_admin', 0)->restore();
});
Route::get( 'udemy/forcedelete', function (){
    $post=Post::onlyTrashed()->where('id', 3)->forcedelete();
});


/*
|--------------------------------------------------------------------------
|contains the "web" middleware group. Now create something great!
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
|
*/
//udemy practice start
//udemy practice start
//udemy practice start


Route::resource('/test', 'TestController');
Route::get('admin/contact/{id}/{name}/{password}', 'TestController@contact');
Route::get('udemy/contact', 'TestController@showScript');


Route::get('udemy/bladefeatures', 'BladefeaturesController@index');


//udemy practice end
//udemy practice end
//udemy practice end




Route::get('/', function () {
    return view('index');
});

Route::get('/admin', function () {
    return view('admin.login');
});

Route::get('/admin/dashboard', 'AdminController@index');

//user route
Route::get('/admin/insertUser', 'usersController@adduser');
Route::get('/admin/userList', 'usersController@userList');


//Profile route
Route::get('/admin/userProfile', 'profileController@viewProfile');
Route::get('/admin/insertUserProfile', 'profileController@insertProfile');



//Route::get('/test/{id}','TestController@index');
//Route::get('/test','TestController@testfunction');
Route::get('/test.{id}', function ($id) {
    return "this is the number: ".$id;
});
Route::get('admin/example/post', array('as'=>'admin.home', function () {
    $url=route('admin.home');
    return "this is the number: ".$url;
}));
