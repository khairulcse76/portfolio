
<div id="footer">
    <div class="container text-center">
        <div class="fnav">
            <p>Copyright &copy; 2017 .Designed by <a href="#" rel="nofollow">Khairul Islam</a></p>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ URL::asset('theme/js/jquery.1.11.1.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('theme/js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('theme/js/SmoothScroll.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('theme/js/easypiechart.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('theme/js/jquery.prettyPhoto.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('theme/js/jquery.isotope.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('theme/js/jquery.counterup.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('theme/js/waypoints.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('theme/js/jqBootstrapValidation.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('theme/js/contact_me.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('theme/js/main.js') }}"></script>
</body>
</html>