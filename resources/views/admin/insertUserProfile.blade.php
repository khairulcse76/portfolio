@extends('admin.layouts.main')
@section('content')
    <div class="insert-main">
        <div class="insert-head">
            <h1>Insert User Profile</h1>
        </div>
        <label class="insert-block">
            <form>
                <input type="text" name="username" placeholder="User Name" required="">
                <input type="text" name="fname:" placeholder="Father's Name:" required="">
                <input type="text" name="mname:" placeholder="Mother's Name:" required="">
                <div class="col-lg-12">
                    <div class="col-lg-6">
                        <label>Gender:</label>
                        <label><input type="radio" name="gender" value="male" checked> Male</label>
                        <label> <input type="radio" name="gender" value="female"> Female</label>
                    </div>
                    <div class="col-lg-6">
                        <label>Religion</label>
                        <select>
                            <option>Religon</option>
                            <option>Religon</option>
                            <option>Religon</option>
                            <option>Religon</option>
                        </select>
                    </div>
                </div>
                 <br><br>
                <label>Date Of Birth
                <input type="date" name="datetime"  min="08/31/2000" required=""></label><br>
                <input type="text" name="nid:" placeholder="National ID" required="">

                 <br><br><br><input type="submit" name="Sign In" value="User || Add">
            </form>
            <div class="sign-down">
                <h4>Already have an account? <a href="login.blade.php"> Login here.</a></h4>
                <h5><a href="{{ '../admin/dashboard' }}">Go Back to Home</a></h5>
            </div>
        </div>
    </div>
@endsection