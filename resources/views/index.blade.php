@extends('layouts.main')
@section('home-content')
    <header id="header">
        <div class="intro">
            <div class="container">
                <div class="row">
                    <div class="intro-text">
                        <h1>Hello,<br> I'm bbangladesh university <span class="name">Khairul Islam</span></h1>
                        <p>Graphics Designer & football match Front End Developer</p>
                        <a href="#about" class="btn btn-default btn-lg page-scroll">About Me</a> </div>
                </div>
            </div>
        </div>
    </header>
@endsection