@include('includes.header')
@yield('home-content')
<!-- Navigation -->
@include('includes.navigation')
<!-- About Section -->
<div id="about">
    <div class="container">
        <div class="section-title text-center center">
            <h2>About Me</h2>
            <hr>
        </div>
        <div class="row">
            <div class="col-md-12 text-center"><img src="theme/img/about-1.jpg" class="img-responsive"></div>
            <div class="col-md-8 col-md-offset-2">
                <div class="about-text">
                    <p>To enter in any organization having the intention to work with honesty, sincerity, to learn, grow and to face the challenges and to gain experience & skill for a successful career.</p>
                    {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam. Sed commodo nibh ante facilisis bibendum dolor feugiat at. Duis sed dapibus leo nec ornare.</p>--}}
                    <p class="text-center"><a class="btn btn-primary" target="__blank" href="https://drive.google.com/open?id=0B6qcbKrMV3pMX0VfN0NIWVViaFE">
                            <i class="fa fa-download"></i> Download Resume</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Skills Section -->
<div id="skills" class="text-center">
    <div class="container">
        <div class="section-title center">
            <h2>Skills</h2>
            <hr>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6 skill"> <span class="chart" data-percent="95"> <span class="percent">95</span> </span>
                <h4>HTML5</h4>
            </div>
            <div class="col-md-4 col-sm-6 skill"> <span class="chart" data-percent="85"> <span class="percent">85</span> </span>
                <h4>CSS3</h4>
            </div>
            <div class="col-md-4 col-sm-6 skill"> <span class="chart" data-percent="80"> <span class="percent">80</span> </span>
                <h4>jQuery</h4>
            </div>
            <div class="col-md-4 col-sm-6 skill"> <span class="chart" data-percent="80"> <span class="percent">80</span> </span>
                <h4>WordPress</h4>
            </div>
            <div class="col-md-4 col-sm-6 skill"> <span class="chart" data-percent="70"> <span class="percent">70</span> </span>
                <h4>Photoshop</h4>
            </div>
            <div class="col-md-4 col-sm-6 skill"> <span class="chart" data-percent="65"> <span class="percent">65</span> </span>
                <h4>Illustrator</h4>
            </div>
        </div>
    </div>
</div>
<!-- Portfolio Section -->
<div id="portfolio">
    <div class="container">
        <div class="section-title text-center center">
            <h2>Portfolio</h2>
            <hr>
        </div>
        <div class="categories">
            <ul class="cat">
                <li>
                    <ol class="type">
                        <li><a href="#" data-filter="*" class="active">All</a></li>
                        <li><a href="#" data-filter=".web">Web Design</a></li>
                        <li><a href="#" data-filter=".app">App Development</a></li>
                        <li><a href="#" data-filter=".branding">Branding</a></li>
                    </ol>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="portfolio-items">
                <div class="col-sm-6 col-md-3 col-lg-3 web">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="theme/img/portfolio/01-large.jpg" title="amar project" rel="prettyPhoto">
                                <div class="hover-text">
                                    <h4>Project Title</h4>
                                    <small>Web Design</small> </div>
                                <img src="theme/img/portfolio/01-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 col-lg-3 app">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="theme/img/portfolio/02-large.jpg" title="Project description" rel="prettyPhoto">
                                <div class="hover-text">
                                    <h4>Project Title</h4>
                                    <small>App Development</small> </div>
                                <img src="theme/img/portfolio/02-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 col-lg-3 web">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="theme/img/portfolio/03-large.jpg" title="Project description" rel="prettyPhoto">
                                <div class="hover-text">
                                    <h4>Project Title</h4>
                                    <small>Web Design</small> </div>
                                <img src="theme/img/portfolio/03-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 col-lg-3 web">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="theme/img/portfolio/04-large.jpg" title="Project description" rel="prettyPhoto">
                                <div class="hover-text">
                                    <h4>Project Title</h4>
                                    <small>Web Design</small> </div>
                                <img src="theme/img/portfolio/04-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 col-lg-3 app">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="theme/img/portfolio/05-large.jpg" title="Project description" rel="prettyPhoto">
                                <div class="hover-text">
                                    <h4>Project Title</h4>
                                    <small>App Development</small> </div>
                                <img src="theme/img/portfolio/05-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 col-lg-3 branding">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="theme/img/portfolio/06-large.jpg" title="Project description" rel="prettyPhoto">
                                <div class="hover-text">
                                    <h4>Project Title</h4>
                                    <small>Branding</small> </div>
                                <img src="theme/img/portfolio/06-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 col-lg-3 branding app">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="theme/img/portfolio/07-large.jpg" title="Project description" rel="prettyPhoto">
                                <div class="hover-text">
                                    <h4>Project Title</h4>
                                    <small>App Development, Branding</small> </div>
                                <img src="theme/img/portfolio/07-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 col-lg-3 web">
                    <div class="portfolio-item">
                        <div class="hover-bg"> <a href="theme/img/portfolio/08-large.jpg" title="Project description" rel="prettyPhoto">
                                <div class="hover-text">
                                    <h4>Project Title</h4>
                                    <small>Web Design</small> </div>
                                <img src="theme/img/portfolio/08-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Achievements Section -->
<div id="achievements" class="text-center">
    <div class="container">
        <div class="section-title center">
            <h2>Some Stats</h2>
            <hr>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-3 wow fadeInDown" data-wow-delay="200ms">
                <div class="achievement-box"> <span class="count">310</span>
                    <h4>Happy Clients</h4>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 wow fadeInDown" data-wow-delay="400ms">
                <div class="achievement-box"> <span class="count">4700</span>
                    <h4>Hours of Work</h4>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 wow fadeInDown" data-wow-delay="600ms">
                <div class="achievement-box"> <span class="count">30</span>
                    <h4>Awards Won</h4>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 wow fadeInDown" data-wow-delay="800ms">
                <div class="achievement-box"> <span class="count">8</span>
                    <h4>Years of Experience</h4>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Resume Section -->
<div id="resume" class="text-center">
    <div class="container">
        <div class="section-title center">
            <h2>Experience</h2>
            <hr>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <ul class="timeline">
                    <li>
                        <div class="timeline-image">
                            <h4>Feb 2013 <br>
                                - <br>
                                Present </h4>
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>Creative Agency</h4>
                                <h4 class="subheading">UX Developer</h4>
                            </div>
                            <div class="timeline-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diamcommodo nibh ante facilisis.</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <h4>Nov 2011 <br>
                                - <br>
                                Jan 2013 </h4>
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>Creative Agency</h4>
                                <h4 class="subheading">Front-end Developer</h4>
                            </div>
                            <div class="timeline-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diamcommodo nibh ante facilisis.</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-image">
                            <h4>Dec 2009 <br>
                                - <br>
                                May 2011 </h4>
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>Creative Agency</h4>
                                <h4 class="subheading">UX Designer</h4>
                            </div>
                            <div class="timeline-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diamcommodo nibh ante facilisis.</p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="section-title center">
            <h2>Education</h2>
            <hr>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <ul class="timeline">

                    <!-- Education Section-->
                    <li>
                        <div class="timeline-image">
                            <h4>2016 <br>
                                - <br>
                                2018 </h4>
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h3>Bachelor Degree</h3>
                                <h4>Bangladesh University</h4>
                                <h4 class="subheading">Computer Science & Engineering</h4>
                            </div>
                            <div class="timeline-body">
                                <p>Now i studying  Bachelor Degree in Bangladesh University. My Bachelor Detree will complete soon</p>
                            </div>
                        </div>
                    </li>
                    <li  class="timeline-inverted">
                        <div class="timeline-image">
                            <h4>2010 <br>
                                - <br>
                                2014 </h4>
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h3>Diploma</h3>
                                <h4>Thakurgaon Polytechnic Institute</h4>
                                <h4 class="subheading">Computer Tecnology </h4>
                            </div>
                            <div class="timeline-body">
                                <p>I have successfully complete 4 years course Diploma in ingineering from Thakurgaon polytechnic institute with
                                    CGPA 3.21 where GPA Scale 4.00</p>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="timeline-image">
                            <h4>2010</h4>
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h3>Secondary School Certificate </h3>
                                <h4>Begun Bari Senior Fazil Madrasah</h4>
                                <h4 class="subheading">Science</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted"> I have successfully complete Secondary School Certificate
                                    examination in 2010 with CGPA 5.00 where GPA Scale 5.00</p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Contact Section -->
@include('includes.contact')
<!-- footer Section -->
@include('includes.footer')